#include "heat_flux_utils.cpp"
#include <demo-heat-flux/binding_factory.h>
#include <demo-heat-flux/heat_assembler.h>
#include <demo-heat-flux/temperature_functor.h>
#include <geom-traits/geom-traits.h>
#include <icmesh/Mesh.h>
#include <loaf/factories.h>
#include <loaf/solve.h>
#include <scheme-lfv/scheme-lfv.h>
#include <scheme-lfv/tpfa.h>

using namespace demo_hf;

inline auto identify_bound_faces(const auto &faces_cells) {
  std::vector<icus::ISet::index_type> boundary_faces_index;
  for (auto &&[f, neigh_c] : faces_cells.targets_by_source()) {
    if (neigh_c.size() != 2) {
      boundary_faces_index.push_back(f);
    }
  }
  return faces_cells.source().extract(boundary_faces_index);
}

auto solve_heat_flux(auto &the_mesh, auto &N, auto &L, auto T_0, auto T_L,
                     auto lambda_0, auto lambda_L) {
  using namespace geom;
  using namespace numscheme;
  using namespace loaf;
  compass::utils::Pretty_printer print;

  auto geom_traits = GeomProperties(the_mesh);
  // bound faces should be given by the mesh
  auto dirichlet_faces = identify_bound_faces(
      the_mesh.template get_connectivity<the_mesh.face, the_mesh.cell>());
  auto tpfa = TPFA(geom_traits, the_mesh.cells, dirichlet_faces);

  auto sites_positions = geom_traits.get_iset_centers(tpfa.sites.location);

  auto rebased_bf = dirichlet_faces.rebase(tpfa.sites.location);

  // in direction non_cst_dir :
  // 0            x_interface                L
  // ------------------|----------------------
  //    lambda_0          lambda_L
  auto &&[lambda, analytical_sol] =
      build_physical_problem(the_mesh, geom_traits, sites_positions, N, L, T_0,
                             T_L, lambda_0, lambda_L);
  auto boundary_values = analytical_sol.extract(rebased_bf);

  // extract lambda info into sites and build controle volumes
  auto Fourier_trans = init_transmissivity(geom_traits, tpfa, lambda);
  auto Xtemp = icus::Field<Temperature>{tpfa.sites};
  auto ns = tpfa.sites.size();
  for (auto &&i : tpfa.sites) {
    // temp varies between 250 +/- 100 K
    Xtemp(i).temperature = 250.0 + i * 200. / (ns - 1) - 100.;
  }
  auto k_grad_T = init_driving_force(Fourier_trans);
  auto heat_assembly = Assembler{tpfa, k_grad_T, rebased_bf, boundary_values};

  // init linear system structures
  auto &&[Jac, RHS] = make_block_linear_system<1>(tpfa.sites);

  // Newton Loop
  auto dt = 0.0; // unnecessary, just to prepare for Newton loop
  // heat_assembly.compute_linear_system(dt, Jac, RHS, Xtemp);
  heat_assembly.compute_linearized_system(dt, Jac, RHS, Xtemp);

  Solver solver;
  solver.compute(Jac);
  auto ls_solution = solver.solve(RHS);
  update_states(Xtemp, ls_solution);

  auto error_norm = distance_to_analytical_solution(Xtemp, analytical_sol);

  assert(error_norm < 1e-10);
  print("Norm of error / N ", error_norm, "\n");

  return error_norm;
}

int main() {

  // Problem parameters
  size_t Nx = 5;
  size_t Ny = 7;
  size_t Nz = 6;
  double Lx = 23.15;
  double Ly = 5.79;
  double Lz = 59.3;
  auto N1D = std::array<size_t, 1>{Nx};
  auto L1D = std::array<double, 1>{Lx};
  auto N2D = std::array<size_t, 2>{Nx, Ny};
  auto L2D = std::array<double, 2>{Lx, Ly};
  auto N3D = std::array<size_t, 3>{Nx, Ny, Nz};
  auto L3D = std::array<double, 3>{Lx, Ly, Lz};
  double T_0 = 210.;
  double T_L = 530.;
  double lambda_0 = 2.3;
  double lambda_L = 0.6;

  // 1D
  auto regular_1dmesh = build_constant_step_mesh(Nx, Lx);
  solve_heat_flux(regular_1dmesh, N1D, L1D, T_0, T_L, lambda_0, lambda_L);
  auto scot_1dmesh = build_scottish_mesh(Nx, Lx);
  solve_heat_flux(scot_1dmesh, N1D, L1D, T_0, T_L, lambda_0, lambda_L);
  // 2D
  auto regular_2dmesh = build_constant_step_mesh(Nx, Ny, Lx, Ly);
  solve_heat_flux(regular_2dmesh, N2D, L2D, T_0, T_L, lambda_0, lambda_L);
  auto scot_2dmesh = build_scottish_mesh(Nx, Ny, Lx, Ly);
  solve_heat_flux(scot_2dmesh, N2D, L2D, T_0, T_L, lambda_0, lambda_L);
  // 3D
  auto regular_3dmesh = build_constant_step_mesh(Nx, Ny, Nz, Lx, Ly, Lz);
  solve_heat_flux(regular_3dmesh, N3D, L3D, T_0, T_L, lambda_0, lambda_L);
  auto scot_3dmesh = build_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz);
  solve_heat_flux(scot_3dmesh, N3D, L3D, T_0, T_L, lambda_0, lambda_L);

  return 0;
}
