#include <Eigen/Dense>
#include <compass-cxx-utils/enumerate.h>
#include <demo-heat-flux/binding_factory.h>
#include <geom-traits/geom-traits.h>
#include <icmesh/Mesh.h>
#include <icus/Field.h>
#include <icus/ISet.h>

auto build_constant_step_mesh(size_t N, double L) {
  std::cout << "Solve over a regular 1D grid with " << N << " cells"
            << std::endl;
  auto cao = icmesh::Constant_axis_offset{0., N, L / N};
  return icmesh::mesh(icmesh::lattice(cao));
}
auto build_constant_step_mesh(size_t Nx, size_t Ny, double Lx, double Ly) {
  std::cout << "Solve over a regular 2D grid with " << Nx << "*" << Ny
            << " cells" << std::endl;
  auto caox = icmesh::Constant_axis_offset{0., Nx, Lx / Nx};
  auto caoy = icmesh::Constant_axis_offset{0., Ny, Ly / Ny};
  return icmesh::mesh(icmesh::lattice(caox, caoy));
}
auto build_constant_step_mesh(size_t Nx, size_t Ny, size_t Nz, double Lx,
                              double Ly, double Lz) {
  std::cout << "Solve over a regular 3D grid with " << Nx << "*" << Ny << "*"
            << Nz << " cells" << std::endl;
  auto caox = icmesh::Constant_axis_offset{0., Nx, Lx / Nx};
  auto caoy = icmesh::Constant_axis_offset{0., Ny, Ly / Ny};
  auto caoz = icmesh::Constant_axis_offset{0., Nz, Lz / Nz};
  return icmesh::mesh(icmesh::lattice(caox, caoy, caoz));
}
auto scottish_step(size_t N, double L) {
  std::vector<double> steps;
  steps.reserve(N);
  double factor = 2 * L / N / (N + 1);
  for (size_t i = 0; i < N; ++i) {
    steps.push_back(factor * (i + 1));
  }
  return steps;
}
auto build_scottish_mesh(size_t N, double L) {
  std::cout << "Solve over a scottish 1D grid with " << N << " cells"
            << std::endl;
  auto steps = scottish_step(N, L);
  auto sao = icmesh::Specific_axis_offset{0., steps};
  return icmesh::mesh(icmesh::lattice(sao));
}
auto build_scottish_mesh(size_t Nx, size_t Ny, double Lx, double Ly) {
  std::cout << "Solve over a scottish 2D grid with " << Nx << "*" << Ny
            << " cells" << std::endl;
  auto x_steps = scottish_step(Nx, Lx);
  auto y_steps = scottish_step(Ny, Ly);
  auto saox = icmesh::Specific_axis_offset{0., x_steps};
  auto saoy = icmesh::Specific_axis_offset{0., y_steps};
  return icmesh::mesh(icmesh::lattice(saox, saoy));
}
auto build_scottish_mesh(size_t Nx, size_t Ny, size_t Nz, double Lx, double Ly,
                         double Lz) {
  std::cout << "Solve over a scottish 3D grid with " << Nx << "*" << Ny << "*"
            << Nz << " cells" << std::endl;
  auto x_steps = scottish_step(Nx, Lx);
  auto y_steps = scottish_step(Ny, Ly);
  auto z_steps = scottish_step(Nz, Lz);
  auto saox = icmesh::Specific_axis_offset{0., x_steps};
  auto saoy = icmesh::Specific_axis_offset{0., y_steps};
  auto saoz = icmesh::Specific_axis_offset{0., z_steps};
  return icmesh::mesh(icmesh::lattice(saox, saoy, saoz));
}

double analytical_solution(double x, double L, const auto &x_I, double T_0,
                           double T_L, double lambda_0, double lambda_L) {
  double T_I = (lambda_0 * ((L - x_I) / L) * T_0 + lambda_L * (x_I / L) * T_L) /
               (lambda_0 * ((L - x_I) / L) + lambda_L * (x_I / L));

  auto eps = 1.e-7;
  if (x_I < eps) {
    return (T_L - T_I) / (L - x_I) * (x - x_I) + T_I;
  } else if (abs(L - x_I) < eps) {
    return ((T_I - T_0) / x_I) * x + T_0;
  } else if (x - x_I < eps) {
    return ((T_I - T_0) / x_I) * x + T_0;
  } else {
    return (T_L - T_I) / (L - x_I) * (x - x_I) + T_I;
  }
}

double distance_to_analytical_solution(const auto &states,
                                       const auto &analytical_sol) {
  assert(states.size() == analytical_sol.size());
  auto N = analytical_sol.size();

  Eigen::VectorXd delta_sol(N);
  for (auto &&i : analytical_sol.support) {
    // states is Temperature Field, thus .temperarure
    delta_sol(i) = states(i).temperature - analytical_sol(i);
  }
  return delta_sol.norm() / N;
}

auto build_physical_problem(const auto &mesh, const auto &geom,
                            const auto &sites_positions, const auto &N,
                            const auto &L, double T_0, double T_L,
                            const auto &lambda_0, const auto &lambda_L) {
  compass::utils::Pretty_printer print;

  // in direction non_cst_dir :
  // 0            x_interface                L
  // ------------------|----------------------
  //    lambda_0          lambda_L
  // random x where lambda_0 != lambda_L
  srand(time(nullptr)); // use current time as seed for random generator
  int non_cst_dir = rand() % mesh.dimension;

  print("T is non constant in the ", non_cst_dir, "direction,");
  print("      there is", N[non_cst_dir], "cells in this direction");

  auto nv = mesh.vertices.size();
  auto x_interface = mesh.vertices(rand() % (nv));
  print("x_interface : ", x_interface);

  // init lambda
  auto cells_center = sites_positions.extract(mesh.cells);
  auto lambda = icus::Field<double>{mesh.cells};
  for (auto &&[i, x] : compass::utils::enumerate(cells_center.span())) {
    if (x[non_cst_dir] < x_interface[non_cst_dir]) {
      lambda(i) = lambda_0;
    } else {
      lambda(i) = lambda_L;
    }
  }
  // get analytical solution
  auto analytical_values = icus::Field<double>{sites_positions.support};
  for (auto &&[i, x] : compass::utils::enumerate(sites_positions.span())) {
    analytical_values(i) = analytical_solution(x[non_cst_dir], L[non_cst_dir],
                                               x_interface[non_cst_dir], T_0,
                                               T_L, lambda_0, lambda_L);
  }

  return std::make_tuple(lambda, analytical_values);
}
