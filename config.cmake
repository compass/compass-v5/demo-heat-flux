set(COMPASS_DEFAULT_GIT_ROOT
    https://gitlab.brgm.fr/brgm/modelisation-geologique/compass
    CACHE STRING
          "The default git repository root to fetch custom dependencies.")

include(FetchContent)
FetchContent_Declare(
  compass-config
  GIT_REPOSITORY ${COMPASS_DEFAULT_GIT_ROOT}/templates/cmake
  GIT_TAG main)

FetchContent_MakeAvailable(compass-config)

set(CMAKE_MODULE_PATH ${compass-config_SOURCE_DIR})

include(compass)
