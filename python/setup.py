import sys

try:
    from skbuild import setup
except ImportError:
    print("scikit-build is required to build from source!", file=sys.stderr)
    print("Install it running: python -m pip install scikit-build")
    sys.exit(1)

package_name = "demo_heat_flux"
package_dir = f"src/{package_name}"

setup(
    name=package_name,
    setup_requires=["setuptools_scm"],
    use_scm_version={
        "write_to": f"{package_dir}/_version.py",
        "relative_to": "../CMakeLists.txt",
    },
    description="demo-heat-flux python bindings",
    author="brgm",
    author_email="compass@brgm.fr",
    license="GPLv3",
    url="https://gitlab.brgm.fr/brgm/modelisation-geologique/compass/demo-heat-flux",
    packages=[package_name],
    package_dir={package_name: package_dir},
    entry_points={
        "icus.build.field": ["bind_demo_fields = demo_heat_flux:bind_demo_fields"]
    },
    long_description="",
    zip_safe=False,
)
