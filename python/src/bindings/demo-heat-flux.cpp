#include <demo-heat-flux/binding_factory.h>
#include <icus/field-binding.h>
#include <nanobind/nanobind.h>

namespace nb = nanobind;

void bind_demo_heat_flux(nb::module_ &module) {
  using namespace demo_hf;
  using namespace icus::binding;

  // enum (cf. icus #104) - define a new struct
  // Temperature_State_field
  bind_field_and_struct<Temperature>(module, "FieldTemp", "Temperature_State",
                                     "bind_demo_fields",
                                     Attribute_info<double>("temperature"));

  auto cls_trans = nb::class_<Trans, numscheme::Scalar_field>(
      module, "Trans_t", "Struct to compute the transmissivity factors");
  cls_trans.def(nb::init<const Trans &>());

  auto cls_d_force = nb::class_<Driving_force>(
      module, "Driving_force", "Struct to compute the driving force");
  cls_d_force.def(nb::init<const Driving_force &>());

  auto cls_assembler = nb::class_<Heat_assembler>(
      module, "Heat_assembler", "Struct to fill the linear system");
  cls_assembler.def(nb::init<const Heat_assembler &>());
  cls_assembler.def(
      nb::init<const numscheme::TPFA &, const Driving_force &,
               const icus::ISet &, const icus::Field<double> &>());

  cls_assembler.def(
      "compute_linear_system",
      &Heat_assembler::compute_linear_system<loaf::OnePhysicsBlockCOO<1>,
                                             loaf::OnePhysicsVector<1>,
                                             icus::Field<Temperature>>);
  cls_assembler.def(
      "compute_linearized_system",
      &Heat_assembler::compute_linearized_system<loaf::OnePhysicsBlockCOO<1>,
                                                 loaf::OnePhysicsVector<1>,
                                                 icus::Field<Temperature>>);

  module.def("make_TPFA", &make_TPFA);
  module.def("init_transmissivity",
             &init_transmissivity<geom::GeomProperties<mesh_3t>>);
  module.def("init_driving_force", &init_driving_force);
  module.def("update_states", &update_states);
}

NB_MODULE(demo_heat_flux, module) {
  // we import icus to be able to handle icus objects on the python side
  // (Scalar_field)
  nb::module_::import_("icus");
  nb::module_::import_("scheme_lfv"); // for make_tpfa

  bind_demo_heat_flux(module);
}
