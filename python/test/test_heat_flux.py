import numpy as np
from demo_heat_flux import (
    Temperature_State,
    make_TPFA,
    init_transmissivity,
    init_driving_force,
    Heat_assembler,
    update_states,
)
from icus import Field, intersect
from icmesh import regular_mesh, scottish_mesh
from loaf.factories import make_block_linear_system
from loaf.eigen_solver import EigenSolver
from geom_traits import GeomProperties, all_boundaries_faces


def step(N, L):
    factor = 2 * L / N / (N + 1)
    return np.array([factor * (i + 1) for i in range(N)])


def build_1D_scottish_mesh(N, L):
    return scottish_mesh(step(N, L))


def build_2D_scottish_mesh(Nx, Ny, Lx, Ly):
    return scottish_mesh(step(Nx, Lx), step(Ny, Ly))


def build_3D_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz):
    return scottish_mesh(step(Nx, Lx), step(Ny, Ly), step(Nz, Lz))


def build_physical_problem(
    the_mesh, geom_prop, sites, N, L, T_0, T_L, lambda_0, lambda_L
):
    eps = 1.0e-7

    def analytical_solution(x, L, x_I, T_0, T_L, T_I):
        if x_I < eps:
            return (T_L - T_I) / (L - x_I) * (x - x_I) + T_I
        elif abs(L - x_I) < eps:
            return ((T_I - T_0) / x_I) * x + T_0
        elif x - x_I < eps:
            return ((T_I - T_0) / x_I) * x + T_0
        else:
            return (T_L - T_I) / (L - x_I) * (x - x_I) + T_I

    non_cst_dir = int(np.random.random() * 3)
    print("T is non constant in the ", non_cst_dir, "direction,")
    print("      there is", N[non_cst_dir], "cells in this direction")
    nv = the_mesh.vertices.shape[0]
    x_interface = the_mesh.vertices[int(np.random.random() * nv)]
    print("x_interface : ", x_interface)
    # build K depending on x_interface[non_cst_dir]
    # in direction non_cst_dir :
    # 0            x_interface                L
    # ------------------|----------------------
    #    lambda_0          lambda_L
    cell_centers = geom_prop.cell_centers
    K = Field(the_mesh.cells, np.float64)
    x_I = x_interface[non_cst_dir]
    for i, x in enumerate(cell_centers):
        if x[non_cst_dir] < x_I:
            K[i] = lambda_0
        else:
            K[i] = lambda_L
    # build analytical solution
    L_I = L[non_cst_dir]
    sites_positions = geom_prop.get_iset_centers(sites.location)
    T_I = (lambda_0 * ((L_I - x_I) / L_I) * T_0 + lambda_L * (x_I / L_I) * T_L) / (
        lambda_0 * ((L_I - x_I) / L_I) + lambda_L * (x_I / L_I)
    )

    analytical_values = Field(sites.location, np.float64)
    for i, x in enumerate(sites_positions):
        analytical_values[i] = analytical_solution(
            x[non_cst_dir], L[non_cst_dir], x_I, T_0, T_L, T_I
        )

    return K, analytical_values


def solve_heat_flux(the_mesh, N, L, T_0, T_L, lambda_0, lambda_L):
    geom = GeomProperties(the_mesh)
    boundary_faces_among_faces = all_boundaries_faces(geom)
    tpfa = make_TPFA(geom, the_mesh.cells, boundary_faces_among_faces)
    K, analytical_sol = build_physical_problem(
        the_mesh, geom, tpfa.sites, N, L, T_0, T_L, lambda_0, lambda_L
    )
    boundary_faces = intersect(
        boundary_faces_among_faces, tpfa.sites.location, tpfa.sites.location
    )
    boundary_values = analytical_sol.extract(boundary_faces)

    Fourier_trans = init_transmissivity(geom, tpfa, K)
    Xtemp = Field(tpfa.sites, Temperature_State)
    # temp varies between 250 +/- 100 K
    ns = Xtemp.size
    for i in tpfa.sites:
        Xtemp[i].temperature = 250.0 + i * 200.0 / (ns - 1) - 100.0  # K

    driving_force = init_driving_force(Fourier_trans)
    heat_assembler = Heat_assembler(
        tpfa, driving_force, boundary_faces, boundary_values
    )

    Jac, RHS = make_block_linear_system(tpfa.sites, 1)  # 1 is block size

    dt = 0.0  # unnecessary, just to prepare for Newton loop
    # heat_assembler.compute_linear_system(dt, Jac, RHS, Xtemp)
    heat_assembler.compute_linearized_system(dt, Jac, RHS, Xtemp)

    solver = EigenSolver()
    solver.set_up(Jac)
    solution, _ = solver.solve(RHS)
    update_states(Xtemp, solution)

    error_norm = (
        np.linalg.norm(Xtemp.as_array().temperature - analytical_sol.as_array()) / ns
    )
    print("Norm of error / N ", error_norm)
    assert error_norm < 1e-10


def test_heat_flux():

    Nx = 5
    Ny = 7
    Nz = 6
    Lx = 23.15
    Ly = 5.79
    Lz = 59.3
    N = [Nx, Ny, Nz]
    L = [Lx, Ly, Lz]
    T_0 = 210.0
    T_L = 530.0
    lambda_0 = 2.3
    lambda_L = 0.6

    print("--------------------------------------")
    print("Solve K grad(T) over a 3D regular grid")
    reg_mesh = regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    solve_heat_flux(reg_mesh, N, L, T_0, T_L, lambda_0, lambda_L)
    print("---------------------------------------")
    print("Solve K grad(T) over a 3D scottish grid")
    scot_mesh = build_3D_scottish_mesh(Nx, Ny, Nz, Lx, Ly, Lz)
    solve_heat_flux(scot_mesh, N, L, T_0, T_L, lambda_0, lambda_L)
