#pragma once
#include <compass-cxx-utils/enumerate.h>
#include <demo-heat-flux/heat_assembler.h>
#include <demo-heat-flux/temperature_functor.h>
#include <geom-traits/geom-traits.h>
#include <icmesh/Mesh.h>
#include <loaf/block_matrix.h>
#include <loaf/vector.h>
#include <scheme-lfv/tpfa.h>

namespace demo_hf {
using namespace icmesh;
using namespace numscheme;
using mesh_3t = Mesh<3, structured_connectivity_matrix<3>>;
using Info = TPFA::Transmissivity_info<double>;
using State_variable = pscal::Variable<pscal::IdMap<Temperature>>;
using Trans = LFVS_transmissivities<TPFA, Info>;
using Driving_force = LFVS_flux<TPFA, Info, State_variable>;
using Heat_assembler = Assembler<TPFA, Driving_force>;

inline numscheme::TPFA make_TPFA(geom::GeomProperties<mesh_3t> &geom_prop,
                                 const icus::ISet &tpfa_cells,
                                 const icus::ISet &tpfa_bound_faces) {
  return numscheme::TPFA(geom_prop, tpfa_cells, tpfa_bound_faces);
}

template <typename Geom_traits>
inline Trans init_transmissivity(Geom_traits &geom_traits,
                                 numscheme::TPFA &tpfa,
                                 const icus::Field<double> &lambda) {
  // extract lambda info into sites and build controle volumes
  auto Fourier_info = tpfa.build_transmissivity_info(lambda, geom_traits);
  auto Fourier_trans = numscheme::LFVS_transmissivities{tpfa, Fourier_info};
  Fourier_trans.update();
  return Fourier_trans;
}

inline Driving_force init_driving_force(Trans &transmissivity) {
  // build flux : lambda grad(T)
  auto temperature_func = init_temperature_functor();
  return numscheme::LFVS_flux{transmissivity, temperature_func};
}

inline void update_states(icus::Field<Temperature> &states,
                          const loaf::OnePhysicsVector<1> &dx) {
  for (auto &&i : states.support) {
    // dx(i) is an ElementaryColBlock
    states(i).temperature += dx(i)[0];
  }
  // flash !
}
} // namespace demo_hf
