#pragma once

#include <compass-cxx-utils/enumerate.h>
#include <compass-cxx-utils/std_ranges.h>
#include <icus/Field.h>
#include <icus/ISet.h>
#include <loaf/elementary_block.h>

namespace demo_hf {
using compass::std_ranges::views::zip;

template <typename Num_scheme, typename Driving_force> struct Assembler {
  const Num_scheme &num_scheme;
  const Driving_force &driving_force;
  // Dirichlet boundary values
  icus::ISet dir_boundary_faces;
  icus::Field<double> dir_boundary_val;

  Assembler(const Num_scheme &scheme, const Driving_force &d_force,
            const icus::ISet &bound_faces, const icus::Field<double> &bound_val)
      : num_scheme{scheme}, driving_force{d_force},
        dir_boundary_faces{bound_faces.rebase(num_scheme.sites.location)},
        dir_boundary_val{bound_val} {
    assert(are_same_indexed_set(dir_boundary_val.support, bound_faces));
    // ok only when there is a single physics
    assert(are_same_indexed_set(num_scheme.sites.location,
                                dir_boundary_faces.parent()));
  }

  template <typename LS_matrix, typename LS_RHS, typename States>
  void compute_linear_system(double dt, LS_matrix &Jac, LS_RHS &RHS,
                             const States &states) {
    using LS_Block = LS_matrix::block_type;
    using LS_ColBlock = LS_RHS::block_type;
    Jac.set_zero();
    RHS = 0.0;
    for (auto &&[flux, tips] :
         compass::utils::enumerate(num_scheme.flux_tips.targets())) {
      auto &&[s0, s1] = tips;
      auto T_flux = driving_force(flux, states);
      // pb with enumerate, so define sk_i and ++sk_i
      //   for(auto &&[sk_i, sk] :
      // compass::utils::enumerate(num_scheme.stencils.targets_by_source(flux)))
      auto sk_i = 0;
      for (auto &&sk : num_scheme.stencils.targets_by_source(flux)) {
        Jac(s0, sk) += LS_Block{T_flux.derivatives[sk_i].temperature};
        Jac(s1, sk) += LS_Block{-T_flux.derivatives[sk_i].temperature};
        ++sk_i;
      }
    }
    // A * x = b
    // Dirichlet BC, modify Jac and fill RHS
    for (auto &&[sb, Tb] :
         zip(dir_boundary_faces.mapping(), dir_boundary_val.span())) {
      Jac.reset_row(sb);
      Jac(sb, sb) += LS_Block{1.0};
      RHS(sb) = LS_ColBlock{Tb};
    }
  }

  template <typename LS_matrix, typename LS_RHS, typename States>
  void compute_linearized_system(double dt, LS_matrix &Jac, LS_RHS &RHS,
                                 const States &states) {
    using LS_Block = LS_matrix::block_type;
    using LS_ColBlock = LS_RHS::block_type;
    Jac.set_zero();
    RHS = 0.0;
    for (auto &&[flux, tips] :
         compass::utils::enumerate(num_scheme.flux_tips.targets())) {
      auto &&[s0, s1] = tips;
      auto T_flux = driving_force(flux, states);
      // pb with enumerate, so define sk_i and ++sk_i
      //   for(auto &&[sk_i, sk] :
      // compass::utils::enumerate(num_scheme.stencils.targets_by_source(flux)))
      auto sk_i = 0;
      for (auto &&sk : num_scheme.stencils.targets_by_source(flux)) {
        Jac(s0, sk) += LS_Block{T_flux.derivatives[sk_i].temperature};
        Jac(s1, sk) += LS_Block{-T_flux.derivatives[sk_i].temperature};
        ++sk_i;
      }
    }

    // Dirichlet BC, modify Jac and fill RHS
    for (auto &&[sb, Tb] :
         zip(dir_boundary_faces.mapping(), dir_boundary_val.span())) {
      Jac.reset_row(sb);
      Jac(sb, sb) += LS_Block{1.0};
      RHS(sb) = LS_ColBlock{Tb};
    }
    // A * (x0 + dx) = b
    // A * dx = b - A * x0 => RHS -= Jac * states
    auto triplets = Jac.coo->get_entries();
    for (auto &&triplet : triplets) {
      RHS(triplet.row()) -=
          LS_ColBlock{triplet.value()[0] * states(triplet.col()).temperature};
    }
  }
};
} // namespace demo_hf
