#pragma once

#include <pscal/variable.h>

namespace demo_hf {

// verbose but necessary for pscal, because LFVS_flux uses a Variable
struct Temperature {
  using value_t = double;
  value_t temperature;
};

inline auto _temperature(const Temperature &T) {
  return pscal::Scalar<Temperature>{T.temperature, Temperature{1.0}};
}

inline auto init_temperature_functor() {
  return pscal::Variable<pscal::IdMap<Temperature>>(_temperature);
}
} // namespace demo_hf
