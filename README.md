Summary:
========

This projet contains an exemple on how to solve the heat flux equation
`K grad(T) = 0 + boundary cond` using the TPFA numerical scheme.

It contains a demo in 1D, 2D and 3D with a (uniform or scottish) cartesian mesh. Lambda is constant by piece, it contains two distinct values. The face which is the interface between the two values is random, as well as the direction in which Lambda is non-constant (so the direction in which the solution T is non-constant).

```
|-------Lambda0--------|----Lambda1----|  
0                 x_interface          L
```


To build:
=========

cmake -B build/linux -DCMAKE_BUILD_TYPE=Debug  
cmake --build build/linux -j $(nproc)
